import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Payment } from '../model/payment';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private httpClient : HttpClient) { }

  getAllEmployee(): Observable<Payment[]> {
    const paymentUrl = 'http://localhost:8080/api/paymentHandler/';
    return this.httpClient.get<Payment[]>(paymentUrl);
  }
}
