import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { Observable } from 'rxjs';
import { Manager } from '../model/manager';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  constructor(private httpClient : HttpClient, private localStorage : LocalStorageService) { }

  addManager(managerData) :Observable<Manager> {
    const managerUrl = 'http://localhost:8080/api/auth/';
    return this.httpClient.post<Manager>(managerUrl, managerData);
  }

  registerManager(managerData) :Observable<Manager> {
    const managerUrl = 'http://localhost:8080/api/manager/';
    return this.httpClient.post<Manager>(managerUrl, managerData);
  }

  isAuthenticated(): Boolean {
    return this.localStorage.retrieve('username') != null;
  }

}
