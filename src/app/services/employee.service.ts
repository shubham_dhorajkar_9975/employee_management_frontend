import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../model/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private httpClient : HttpClient) { }

  addEmployee(employeeBody): Observable<Employee> {
    const employeeUrl = 'http://localhost:8080/api/employees/';
    return this.httpClient.post<Employee>(employeeUrl, employeeBody);
  }

  getAllEmployee(): Observable<Employee[]> {
    const employeeUrl = 'http://localhost:8080/api/employees/';
    return this.httpClient.get<Employee[]>(employeeUrl);
  }

  viewEmployee(categoryId): Observable<Employee> {
    const employeeUrl = 'http://localhost:8080/api/employees/' + categoryId;
    return this.httpClient.get<Employee>(employeeUrl);
  }

  updateEmployee(id, employeeBody): Observable<Employee> {
    const employeeUrl = 'http://localhost:8080/api/employees/' + id;
    return this.httpClient.put<Employee>(employeeUrl, employeeBody);
  }

  deleteCategory(id): Observable<Employee> {
    const employeeUrl = 'http://localhost:8080/api/employees/' + id;
    return this.httpClient.delete<Employee>(employeeUrl);
  }

}
