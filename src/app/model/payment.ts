export class Payment {
    id : number;
    email : String;
    name : String;
    phone : String;
    productInfo : String;
    amount : any;
    paymentStatus : any;
    paymentDate : Date;
}
