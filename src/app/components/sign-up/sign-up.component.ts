import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ManagerService } from 'src/app/services/manager.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private managerService : ManagerService, private router : Router) { }

  ngOnInit(): void {
  }

  signUp(form){
    let managerData = {
      name : form.value.name,
      username : form.value.username,
      password : form.value.password
    }
    this.managerService.registerManager(managerData).subscribe(data => {
      console.log(data);
      this.router.navigateByUrl("/sign-in");
    })
    
  }
}
