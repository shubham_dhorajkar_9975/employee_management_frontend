import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { ManagerService } from 'src/app/services/manager.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor(private managerService : ManagerService, private router : Router, private localStorage : LocalStorageService) { }

  valid : number = 0;

  ngOnInit(): void {
  }

  login(form){
    let managerData = {
      username : form.value.username,
      password : form.value.password
    }
    this.managerService.addManager(managerData).subscribe(data =>{
      console.log(data);
      if(data==1){
        this.localStorage.store("username",form.value.username);        
        this.router.navigateByUrl("/home");
      }
      else
      {
        this.localStorage.clear("username");
        this.valid=1;
      }
    })
  }

}
