import { Component, OnInit } from '@angular/core';
import { Payment } from 'src/app/model/payment';
import { PaymentService } from 'src/app/services/payment.service';

@Component({
  selector: 'app-payment-dashboard',
  templateUrl: './payment-dashboard.component.html',
  styleUrls: ['./payment-dashboard.component.css']
})
export class PaymentDashboardComponent implements OnInit {

  constructor(private paymentService : PaymentService) { }

  payments : Payment[];

  ngOnInit(): void {
    this.paymentService.getAllEmployee().subscribe(data =>{
      this.payments = data;
    })
  }

}
