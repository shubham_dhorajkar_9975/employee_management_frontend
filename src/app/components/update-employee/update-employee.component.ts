import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from 'src/app/model/employee';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {

  id : number;
  employee : Employee;
  constructor(private activatedRoute : ActivatedRoute, private employeeService : EmployeeService, private router : Router) { }

  ngOnInit(): void {
    this.id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.employeeService.viewEmployee(this.id).subscribe(data =>{
      this.employee = data;
    })
  }

  updateEmployee(form){
    let empData = {
      firstName : form.value.firstName,
      lastName : form.value.lastName,
      address : form.value.address,
    }

    this.employeeService.updateEmployee(this.id, empData).subscribe(data =>{
      this.router.navigateByUrl("/manager-home");
    })
  }
}
