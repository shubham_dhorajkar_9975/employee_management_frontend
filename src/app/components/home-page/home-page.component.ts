import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  amazon : number = 199;
  zee : number = 149;
  netflix : number = 249;

  constructor(private router : Router) { }

  ngOnInit(): void {
  }
  subscribeNetflix(){

    this.router.navigate(['payment', "NETFLIX","199"]);

  }
  subscribeZee(){
    this.router.navigate(['payment', "ZEE5","149"]);

  }
  subscribeAmazon(){
    this.router.navigate(['payment', "AMAZON PRIME VIDEO","199"]);
  }

}
