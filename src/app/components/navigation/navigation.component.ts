import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { ManagerService } from 'src/app/services/manager.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(public managerService : ManagerService,private router : Router, private localStorage : LocalStorageService) { }

  ngOnInit(): void {
  }
  logOut()
  {
    this.localStorage.clear('username');
    this.router.navigateByUrl("/home");
  }

}
