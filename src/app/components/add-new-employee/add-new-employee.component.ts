import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-add-new-employee',
  templateUrl: './add-new-employee.component.html',
  styleUrls: ['./add-new-employee.component.css']
})
export class AddNewEmployeeComponent implements OnInit {

  constructor(private employeeService : EmployeeService, private router : Router) { }

  ngOnInit(): void {
  }

  addNewEmployee(form){

    let empData = {
      firstName : form.value.firstName,
      lastName : form.value.lastName,
      address : form.value.address,
    }

    this.employeeService.addEmployee(empData).subscribe(data => {
      this.router.navigateByUrl("/manager-home");
    })
  }

}
