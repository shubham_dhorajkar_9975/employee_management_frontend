import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { ManagerHomePageComponent } from './components/manager-home-page/manager-home-page.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { AddNewEmployeeComponent } from './components/add-new-employee/add-new-employee.component';
import { UpdateEmployeeComponent } from './components/update-employee/update-employee.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { FormsModule, NgForm, ReactiveFormsModule } from '@angular/forms';
import {NgxWebstorageModule} from 'ngx-webstorage';
import { NavigationComponent } from './components/navigation/navigation.component';
import { PaymentComponent } from './components/payment/payment.component';
import { PaymentDashboardComponent } from './components/payment-dashboard/payment-dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    ManagerHomePageComponent,
    SignInComponent,
    SignUpComponent,
    AddNewEmployeeComponent,
    UpdateEmployeeComponent,
    EmployeeListComponent,
    NavigationComponent,
    PaymentComponent,
    PaymentDashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxWebstorageModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
