import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from './auth-guard.guard';
import { AddNewEmployeeComponent } from './components/add-new-employee/add-new-employee.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { ManagerHomePageComponent } from './components/manager-home-page/manager-home-page.component';
import { PaymentDashboardComponent } from './components/payment-dashboard/payment-dashboard.component';
import { PaymentComponent } from './components/payment/payment.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { UpdateEmployeeComponent } from './components/update-employee/update-employee.component';


const routes: Routes = [
  { path: 'home', component: HomePageComponent },
  { path: 'sign-in', component: SignInComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'create-employee', component: AddNewEmployeeComponent, canActivate : [AuthGuardGuard] },
  { path: 'update-employee/:id', component: UpdateEmployeeComponent ,canActivate : [AuthGuardGuard]},
  { path: 'payment-dashboard', component: PaymentDashboardComponent ,canActivate : [AuthGuardGuard]},
  { path: 'payment/:subscription/:price', component: PaymentComponent },
  { path: 'employee-list', component: EmployeeListComponent ,canActivate : [AuthGuardGuard]},
  { path: 'manager-home', component: ManagerHomePageComponent ,canActivate : [AuthGuardGuard]},
  { path: '', component: HomePageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
