import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { ManagerService } from './services/manager.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor(
    private signInService: ManagerService,
    private router: Router
  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ):
     | Observable<boolean | UrlTree> 
     | Promise<boolean | UrlTree> 
     | boolean 
     | UrlTree {
      let isActivate = this.signInService.isAuthenticated();
      if (isActivate) {
        return true;
      } else {
        this.router.navigateByUrl('/sign-in');
      }
  }
  
}
